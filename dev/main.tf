provider "aws"{
    region = var.region
}


module "sagemaker" {
  source = "../modules/sagemaker"
  instance_type = var.instance_type
  notebook_instance_name = var.notebook_instance_name
  region = var.region

}
