#Creation of Notebook

provider "aws"{
    region = var.region
}

resource "aws_sagemaker_notebook_instance" "sagemaker-ni" {
  name          = var.notebook_instance_name
  role_arn      = aws_iam_role.sagemaker_role.arn
  instance_type = var.instance_type

}

#Sagemaker role which notebook instance assumes
resource "aws_iam_role" "sagemaker_role" {
  name = "sagemaker_role"
  # count = var.sagemaker_enable == true ? 1 : 0
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "sagemaker.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

